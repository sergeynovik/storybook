import { Meta, Story } from '@storybook/angular';
import { action } from '@storybook/addon-actions';
import { TestButtonComponent } from './../app/components/test-button/test-button.component';

export default {
  title: 'Components/Test button',
  component: TestButtonComponent,
  props: {
    clickEvent: (e) => {
      e.preventDefault();
      action('log')(e.target);
    },
  },
  parameters: {
    docs: {
      description: {
        component: 'Test button for try Storybook',
      },
    },
  },
  argTypes: {
    background: {
      control: {
        type: 'color',
      },
      table: {
        category: 'Colors',
      },
    },
    invert: {
      table: {
        category: 'Colors',
      },
    },
    size: {
      control: {
        type: 'select',
      },
      table: {
        category: 'Sizes',
      },
    },
  },
} as Meta;

const Template: Story = (args) => ({
  props: args,
});

export const Big = Template.bind({});
Big.args = {
  size: 'big',
  invert: true,
  background: 'green',
};

export const Medium = Template.bind({});
Medium.args = {
  size: 'medium',
  invert: false,
};

export const Small = Template.bind({});
Small.args = {
  size: 'small',
  invert: false,
};
