import { Story } from '@storybook/angular/types-6-0';
import { Meta } from '@storybook/angular';
import { CustomLabelComponent } from './../app/components/custom-label/custom-label.component';

export default {
  title: 'Components/Custom label',
  component: CustomLabelComponent,
  parameters: {
    docs: {
      description: {
        component: 'Custom label',
      },
    },
  },
  argTypes: {
    text: {
      defaultValue: '',
      description: 'Text of label',
      control: {
        type: 'text',
      },
      table: {
        category: 'Text',
      },
    },
    bold: {
      defaultValue: false,
      description: 'Is bold',
      control: {
        type: 'boolean',
      },
      table: {
        category: 'Style',
      },
    },
    pixelSize: {
      defaultValue: false,
      description: 'Size in pixels',
      control: {
        type: 'number',
      },
      table: {
        category: 'Style',
      },
    },
  },
} as Meta;

const Template: Story = (args) => ({
  props: args,
});

export const Title = Template.bind({});
Title.args = {
  text: 'Custom Title',
  bold: true,
  pixelSize: 16,
};

export const Helper = Template.bind({});
Helper.args = {
  text: 'Help text',
  pixelSize: 12,
};
