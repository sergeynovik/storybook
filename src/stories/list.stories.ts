import { CommonModule } from '@angular/common';
import { Meta, Story, moduleMetadata } from '@storybook/angular';
import { ListComponent } from './../app/components/list/list.component';
import { ListItemComponent } from './../app/components/list/components/list-item/list-item.component';
import { CustomLabelComponent } from './../app/components/custom-label/custom-label.component';

export default {
  title: 'Multi components/List',
  component: ListComponent,
  subcomponents: { ListItemComponent, CustomLabelComponent },
  decorators: [
    moduleMetadata({
      declarations: [ListComponent, ListItemComponent, CustomLabelComponent],
      imports: [CommonModule],
    }),
  ],
  parameters: {
    docs: {
      description: {
        component: 'List of items',
      },
    },
  },
  argTypes: {
    list: {
      defaultValue: [],
      description: 'Items as array',
      control: {
        type: 'array',
      },
      table: {
        category: 'Items',
      },
    },
    titleText: {
      defaultValue: 'Items',
      description: 'Title of list',
      control: {
        type: 'text',
      },
      table: {
        category: 'Title',
      },
    },
    titleBold: {
      defaultValue: true,
      description: 'Is title bold',
      control: {
        type: 'boolean',
      },
      table: {
        category: 'Title',
      },
    },
    titleSize: {
      defaultValue: 18,
      description: 'Size of title',
      control: {
        type: 'number',
      },
      table: {
        category: 'Title',
      },
    },
    footerText: {
      defaultValue: 'No items',
      description: 'If have no items',
      control: {
        type: 'text',
      },
      table: {
        category: 'Footer',
      },
    },
    footerBold: {
      defaultValue: false,
      description: 'Is footer bold',
      control: {
        type: 'boolean',
      },
      table: {
        category: 'Footer',
      },
    },
    footerSize: {
      defaultValue: 12,
      description: 'Size of footer',
      control: {
        type: 'number',
      },
      table: {
        category: 'Footer',
      },
    },
  },
} as Meta;

const Template: Story = (args) => ({
  props: args,
});

export const Items = Template.bind({});
Items.args = {
  titleText: 'Items',
  list: ['Item 1', 'Item 2'],
};

export const Empty = Template.bind({});
Empty.args = {};
