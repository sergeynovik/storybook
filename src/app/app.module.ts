import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { TestButtonComponent } from './components/test-button/test-button.component';
import { ListComponent } from './components/list/list.component';
import { ListItemComponent } from './components/list/components/list-item/list-item.component';
import { CustomLabelComponent } from './components/custom-label/custom-label.component';

@NgModule({
  declarations: [AppComponent, TestButtonComponent, ListComponent, ListItemComponent, CustomLabelComponent],
  imports: [BrowserModule, AppRoutingModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
