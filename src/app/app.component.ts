import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  public obj = null;

  public receiveData(obj: any): void {
    this.obj = obj;
  }
}
