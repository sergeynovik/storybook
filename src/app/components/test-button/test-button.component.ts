import { map, mergeMap, take } from 'rxjs/operators';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { forkJoin, Observable, of } from 'rxjs';

@Component({
  selector: 'app-test-button',
  templateUrl: 'test-button.component.html',
  styleUrls: ['test-button.component.scss'],
})
export class TestButtonComponent {
  @Input()
  public background = '';
  @Input()
  public invert = false;
  @Input()
  public size: 'small' | 'medium' | 'big' = 'medium';
  @Output()
  public clickEvent = new EventEmitter();

  public click(): void {
    const testArr: Array<Obj> = [
      {
        data: `[1] ${new Date().toTimeString()}`,
        arr: ['str11', 'str12'],
      },
      {
        data: `[2] ${new Date().toTimeString()}`,
        arr: ['str21', 'str22', 'str23'],
      },
      {
        data: `[3] ${new Date().toTimeString()}`,
        arr: ['str31'],
      },
    ];
    of(testArr)
      .pipe(
        take(1),
        map((objArr) =>
          forkJoin([
            of(objArr),
            forkJoin([
              ...objArr.map((obj) =>
                forkJoin([...obj.arr.map((str) => this.proc(str))])
              ),
            ]),
          ])
        ),
        mergeMap((data) => forkJoin([data])),
        map(([data]) =>
          data[0].map((item, index) => ({
            ...item,
            arr: data[1][index],
          }))
        )
      )
      .subscribe((data) => this.clickEvent.next(data));
  }

  private proc(str: string): Observable<string> {
    return of(`NEW ${str}`);
  }
}

interface Obj {
  data: string;
  arr: string[];
}
