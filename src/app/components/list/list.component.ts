import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
})
export class ListComponent {
  @Input()
  public list = [];
  @Input()
  public titleText = 'Items';
  @Input()
  public titleBold = true;
  @Input()
  public titleSize = 18;
  @Input()
  public footerText = 'No items';
}
