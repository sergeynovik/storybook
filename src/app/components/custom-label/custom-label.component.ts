import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'app-custom-label',
  templateUrl: './custom-label.component.html',
  styleUrls: ['./custom-label.component.scss'],
})
export class CustomLabelComponent {
  @Input()
  public text;
  @Input()
  public bold = false;
  @Input()
  public pixelSize = 12;
}
